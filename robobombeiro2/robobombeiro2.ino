#include <Ultrasonic.h>

#define frenteEchoPin  5
#define frenteTriggerPin  6
#define esquerdaEchoPin  7
#define esquerdaTriggerPin  8
#define direitaEchoPin  9
#define direitaTriggerPin 10
const int motorEsquerdoDir1 = 13;//não são 2 motor apenas as duas ligações que o motor tem.
const int motorEsquerdoPwm2 = 11;
const int motorDireitoDir1 = 12;
const int motorDireitoPwm2 = 3;
const int ventoinha=2;

float tempoReacao=20*58; //58micro segundos por centimetro, 20= cm
 
Ultrasonic ultrasonic_frente(frenteTriggerPin, frenteEchoPin);
Ultrasonic ultrasonic_esquerda(esquerdaTriggerPin, esquerdaEchoPin);
Ultrasonic ultrasonic_direita(direitaTriggerPin, direitaEchoPin);
float maxDistanciaFrente=25.00;
float maxDistanciaDireita=20.00;
float maxDistanciaEsquerda=20.00;

void setup() {
  // serial
  Serial.begin(9600);

  // motores
  pinMode(motorEsquerdoDir1, OUTPUT);
  pinMode(motorEsquerdoPwm2, OUTPUT);
  pinMode(motorDireitoDir1, OUTPUT);
  pinMode(motorDireitoPwm2, OUTPUT);

  pinMode(ventoinha, OUTPUT);
  int sensorChama = analogRead(A0);

}


void loop() {

  // Para a frente
 
  if( ultrasonic_frente.distanceRead(CM) < maxDistanciaFrente)
        {deslocaFrente();
    Serial.println(" Aviso de Próximidade - Frente ");
    Serial.println(ultrasonic_frente.distanceRead(CM));
    delay(200); //
   
     ultrasonic_esquerda.distanceRead(CM);
      delay(200); // pausar o programa 20 milisegundos
       ultrasonic_direita.distanceRead(CM);  
        delay(200); // pausa o programa 20 milisegundos
          {if (ultrasonic_esquerda.distanceRead(CM) < ultrasonic_direita.distanceRead(CM)) // se distancia da esquerda é menor di que a direita, anda a direita
         {delay(300); 
          deslocaTras();
          //deslocaDireita();
         }}}
         /*else if (ultrasonic_esquerda.distanceRead(CM) > ultrasonic_direita.distanceRead(CM)) // se a esquerda é maior do que a direita, anda a esquerda, anda a maior
         {
          deslocaEsquerda();
         }
        }
        else 
        {
        Serial.println("Caminho Livre");
         deslocaFrente(); // Seguir para a frente
        }

         /* Para a esquerda

          sensorEsquerda();
         if(sensorEsquerda < maxDistanciaEsquerda)
         {
          Serial.println(" Aviso de Próximidade - Esquerda ");
           delay(20); // atrasa o programa 20 milisegundos
          sensorFrente(); // ou esquerda?
           delay(20);
           sensorDireita();
           delay(20);
          if(sensorFrente < sensorDireita) // frente<->esquerda?
          {
            deslocaDireita();
          }
          else if (sensorFrente > sensorDireita)
          {
            deslocaFrente();
          }
          }

          // Para a direita

           sensorDireita();
          if(sensorDireita < maxDistanciaDireita)
          {
            Serial.println(" Aviso de Próximidade - Direita ");
             delay(20);
            sensorFrente(); // Direita?
             delay(20);
            sensorEsquerda();
             delay(20);
            if(sensorEsquerda < sensorFrente){
              deslocaFrente();
            }
            else if( sensorEsquerda > sensorFrente){
              deslocaEsquerda();
            }
           }
        */  

//sensorFrente();
//sensorEsquerda();
//sensorDireita();
}

/*
void testeDistanciaFrente(){
  digitalWrite(frenteTrigPin, LOW); // para gerar um impulso limpo usa-se LOW, com 4 microsegundos
  delayMicroseconds(4);
  digitalWrite(frenteTrigPin, HIGH);
  delayMicroseconds(10); // Trigger de 10 microsegundos
  digitalWrite(frenteTrigPin, LOW);
  duracaoFrente = pulseIn(frenteEchoPin, HIGH); // medir o tempo entre impulsos em microsegundos
  distanciaFrente = duracaoFrente * 10 / 292 / 2; // converter a distancia em cm
  Serial.print(" Distância Frente: ");
  Serial.print(distanciaFrente);
  Serial.println(" cm "); 
  }

void testeDistanciaEsquerda(){
  digitalWrite(esquerdaTrigPin, LOW); // para gerar um impulso limpo usa-se LOW, com 4 microsegundos
  delayMicroseconds(4);
  digitalWrite(esquerdaTrigPin, HIGH);
  delayMicroseconds(10); // Trigger de 10 microsegundos
  digitalWrite(esquerdaTrigPin, LOW);
  duracaoEsquerda = pulseIn(esquerdaEchoPin, HIGH); // medir o tempo entre impulsos em microsegundos
  distanciaEsquerda = duracaoEsquerda * 10 / 292 / 2; // converter a distancia em cm
  Serial.print(" Distância Esquerda: ");
  Serial.print(distanciaEsquerda);
  Serial.println(" cm "); 
  
  }

void testeDistanciaDireita(){
  digitalWrite(direitaTrigPin, LOW); // para gerar um impulso limpo usa-se LOW, com 4 microsegundos
  delayMicroseconds(4);
  digitalWrite(direitaTrigPin, HIGH);
  delayMicroseconds(10); // Trigger de 10 microsegundos
  digitalWrite(direitaTrigPin, LOW);
  duracaoDireita = pulseIn(direitaEchoPin, HIGH); // medir o tempo entre impulsos em microsegundos
  distanciaDireita = duracaoDireita * 10 / 292 / 2; // converter a distancia em cm
  Serial.print(" Distância Direita: ");
  Serial.print(distanciaDireita);
  Serial.println(" cm "); 
  
  } 
*/


void sensorDireita(){
  float sensor_direita;
 Serial.print("Sensor direita: ");
  Serial.print(ultrasonic_direita.distanceRead(CM)); // mostra a distancia a esquerda
  Serial.println("cm");

  
}

void sensorEsquerda(){
  float sensor_esquerda;
 Serial.print("Sensor esquerda: ");
  Serial.print(ultrasonic_esquerda.distanceRead(CM)); // mostra a distancia a esquerda
  Serial.println("cm");
}

void sensorFrente(){
  float sensor_frente;
 Serial.print("Sensor Frente: ");
  Serial.print(ultrasonic_frente.distanceRead(CM)); // Prints the distance making the unit explicit
  Serial.println("cm");
  }

void ventoinhaChama(){
if (analogRead(A0)>=750){
detetarChama();

delay (3000);
apagarChama();

}

else{ if(analogRead(A0)<=200) {

  
 naoDetetarChama();
        }
    }  
}

void naoDetetarChama() {
  Serial.println("Não deteta chama.");
int sensorChama = analogRead(A0);
  digitalWrite(ventoinha, 0);
}


void detetarChama() {
  Serial.println("Detetar chama.");
int sensorChama = analogRead(A0);
  Serial.println(sensorChama);
}
void apagarChama() {
  Serial.println("apagar chama.");
  
  digitalWrite(ventoinha, HIGH);
}
void deslocaTras() {
  Serial.println("para trás.");
  digitalWrite(motorEsquerdoDir1, LOW);
  digitalWrite(motorEsquerdoPwm2, HIGH);
  digitalWrite(motorDireitoDir1, LOW);
  digitalWrite(motorDireitoPwm2, HIGH);
}
void deslocaFrente() {
  Serial.println("Frente.");
  digitalWrite(motorEsquerdoDir1, HIGH);
  digitalWrite(motorEsquerdoPwm2, HIGH);
  digitalWrite(motorDireitoDir1, HIGH);
  digitalWrite(motorDireitoPwm2, HIGH);
}
void deslocaEsquerda() {
  Serial.println("Esquerda.");
  digitalWrite(motorEsquerdoDir1, LOW);
  digitalWrite(motorEsquerdoPwm2, HIGH);
  digitalWrite(motorDireitoDir1, HIGH);
  digitalWrite(motorDireitoPwm2, HIGH);
}
void deslocaDireita() {
  Serial.println("Direita.");
  digitalWrite(motorEsquerdoDir1, HIGH);
  digitalWrite(motorEsquerdoPwm2, HIGH);
  digitalWrite(motorDireitoDir1, LOW);
  digitalWrite(motorDireitoPwm2, HIGH);
}
